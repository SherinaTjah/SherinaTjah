##Week 1 
-----------
######Woensdag 30 augustus
**Het begin**, vandaag werden er projectgroepen gevormd het was nog erg wennen want ik kende niemand uit mijn projectgroep. Naar mijn gevoel waren we al snel aan elkaar gewend. Vervolgens werden we gelijk in het diepe gegooid en kregen wij een uurtje de tijd voor onze eerste opdracht namelijk: “*Maak een team logo*”. Erg leuk je kon toen gelijk zien hoe alle groepsleden functioneerde binnen het team. 

######Donderdag 31 augustus
**Kick off Design Challenge**, vandaag vond de kick off van ons eerste project plaats namelijk de: “*Design Challenge*”. Hier kregen wij informatie over wat ons te wachten staat tijdens dit project. Vervolgens kregen wij onze eerste hoorcollege. Met als thema “games”. Hier hebben we geleerd wat “*the meaningful play*” van een game inhoudt. 

Dit is namelijk dat iemand die het speelt iets mee neemt uit de spel-ervaring. Dit houdt in dat er sprake is van dat wat in de realiteit van het spel gebeurt vertaald wordt in het dagelijks leven. 

##Week 2 
-------
######Maandag 4 september 
**Extra informatie**, vandaag kregen wij wat inhoudelijke informatie betreft de design challenge . We kregen de briefing en vanaf dit punt zijn we zelf van start gaan. Mijn groep en ik begonnen eigenlijk al snel ver vooruit te denken over wat voor spel we zouden gaan maken en vooral hoe we het online component erin konden verwerken.  Na heel wat brainstormen over het spel zei onze projectcoach dat we beter een stapje terug konden nemen. We moesten namelijk eerst beginnen met het thema van het spel en de doelgroep. Hier zijn wij toen over gaan nadenken en hebben het thema “*Rotterdam* “ gekozen. Dit hebben wij toen onderverdeeld in: “*Sport, Kunst, Cultuur, Architectuur en het uitgaansleven*”.  We spraken af om de moodboards en het visuele onderzoek woensdag af te hebben zodat we elkaar feedback konden geven en dit konden verwerken in onze individuele stukken. 

######Dinsdag 5 september
What is a Design, vandaag kregen wij een hoorcollege over wat een design nou eigenlijk is. Een van de belangrijkste dingen die we geleerd hebben vandaag is te verwoorden in één zin: “*design(1) is to design(2) a design(3) to produce a design(4)*’.  

**1.** Aan het begin van de zin wordt design verwezen naar de beroepspraktijk 
    (naar de functies en banen). 
**2.** Daarna wordt het woord design verwezen naar de actie het creëren van. 
**3.** Vervolgs wordt er verwezen naar het concept het idee. 
**4.** En tot slot wordt design gebruikt als het resultaat (object of dienst). 

Een ander belangrijk punt was  het ontwerpproces van CMD Rotterdam. 
Elk project verloopt dus in drie fases Iteraties genoemd. De letterlijke vertaling van Iteraties is eigenlijk niets anders dan een herhaling. Je herhaalt eigenlijk je product en/of dienst. Dit doe je door middel van een onderzoek --> concept --> prototype. 

En dit begint met ontwerpvragen en eindigt met ontwerp resultaten. Dit wordt weergegeven in een cirkel, om aan te geven dat als je uiteindelijk je ontwerp resultaat hebt, je na alle feedback die je tijdens elke iteratie krijgt de ontwerpvragen altijd aan te kunnen passen en zo het product of de aangeboden dienst kunt verbeteren. In de volgende iteratie. 

######Woensdag 6 september
**Moodboard en de Doelgroep**, afgelopen maandag hebben mijn projectteam en ik gebrainstormd over wat voor spel wij wilde ontwerpen. We liepen eerst te snel op de zaken vooruit en zijn toen maar even een stapje terug gegaan. Maar vandaag konden wij wel van start gaan en bedenken wat voor game wij nu eigenlijk wilde ontwerpen. Wij zijn een nieuwe brainstorm sessie begonnen betreft de game onderwerpen. Wij hadden enkele ideeën en zijn toen uitgekomen op een bordspel met als online component een draaiend rad. We hebben dat vervolgens even naast ons neergelegd. En gaven elkaar feedback op de gemaakte moodboards en op de thema onderzoeken. Ook kwam de projectcoach erbij en gaf ons nog extra tips en tricks. Zo kwam ik er bijvoorbeeld achter dat mijn thema veelste breed was en ik het eigenlijk wat specifieker moest maken. Wij spraken toen af dat we onze onderzoeken zouden gaan aanpassen met de gekregen feedback en dat we maandag 11 september echt van start konden gaan met de inhoud van ons spel en het paperprototype. 

######Donderdag 7 september
Visualiseer je gedachtes, Leuk! Het eerste werkcollege is achter de rug. We kregen de opdracht een stappenplan te beschrijven van een zelf georganiseerd feest. Dit moesten we doen door zo min mogelijk tekst te gebruiken en zoveel mogelijk visuele beelden te gebruiken. 

------
##Week 3 
######Maandag 11 september
Game-inhoud, vandaag zijn wij van start gegaan met de inhoud van ons bordspel. We zijn voornamelijk bezig geweest met het bedenken van vragen en opdrachten en de spelregels. 

Het papieren prototype was bij ons niet erg ingewikkeld om uit te werken. We zijn dus voornamelijk bezig geweest met de inhoudelijke componenten van de game die waren dus iets belangrijker. Hierdoor werd het ook mogelijk het spel te kunnen spelen.  We waren voornamelijk gezamenlijk bezig met de dingen die het meeste tijd in beslag namen. Het online component is door een van de projectleden tot zich genomen die affiniteit had met het opzetten van websites. 

######Dinsdag 12 september
**Verbeelden**, Het hoorcollege over het verbeelden. Wat vertellen al die beelden nou die wij dagelijks tegenkomen. Je zult je verbazen maar ze vertellen je veel meer dan je denkt. Tijdens dit college heb ik geleerd dat een beeld eigenlijk  alle communicatiemiddelen zijn die geen primaire tekst bevatten en dat zij d.m.v. een twee dimensionaal medium tot ons komen en als primair doel hebben communicatief-retorisch te zijn. Dit is eigenlijk niets anders dan overtuigen. 

Verder heb ik vandaag geleerd een blog online te zetten. Je kunt de tekst vormgeven op een online platform genaamd :”#StackEdit”. Dit kun je vervolgens opslaan als een mark down bestand. En door gebruik te maken van het programma smartgit kun je dit online krijgen op je blog. Dit programma vertaald eigenlijk alles naar html zodat het leesbaar is op een online toegankelijk platform. 

######Woensdag  13 september
Presenteren, vandaag hebben wij het spel en het prototype afgemaakt en was het tijd om het gaan te presenteren aan de klas. Dit was erg handig zo kon je zien wat alle andere teams hadden gedaan en hoe zij de bepaalde dingen hadden aangepakt. Zo krijg je feedback op je project en je krijgt ook nieuwe inzichten van de ideeën van andere. Als top kreeg ons team te horen dat het er wel leuk uitzag en als tip gaven ze ons mee dat we moesten proberen het thema meer tot uiting te laten komen. 

######Donderdag 14 september 
SpelAnalyse, Spannend we mogen ons prototype zelf gaan uit testen! Dit heeft geleid tot vele nieuwe inzichten en een aantal verbeter punten die we zeker gaan meenemen in de volgende iteratie. Het was ook erg leuk om samen met mijn team te spelen, we hebben erg gelachen om onze eigen opdrachten. Vandaag hebben we ook de groepsdocumenten afgerond. Zodat we het alleen nog hoeven na te lezen en morgen kunnen gaan inleveren. 

